﻿using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace OnlineMobileShopManagementSystem.DAL
{
    public class dalUser
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        private static readonly string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        bool status = false;

        internal bool addAdmin(daoUser daoUser)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into userdetail(Name,UserName,Password,Gender,PhoneNumber,userGroupId) values(@Name,@UserName,@Password,@Gender,@PhoneNumber,@userGroupId)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@Name", daoUser.name);
                        cmd.Parameters.AddWithValue("@UserName",daoUser.UserName);
                        cmd.Parameters.AddWithValue("@Password",daoUser.password);
                        cmd.Parameters.AddWithValue("@Gender",daoUser.gender);
                        cmd.Parameters.AddWithValue("@PhoneNumber",daoUser.phone);
                        cmd.Parameters.AddWithValue("@userGroupId",daoUser.adminValue);

                        int RowAffected=cmd.ExecuteNonQuery();
                        if (RowAffected > 0)
                        {
                            status = true;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return status;
        }

        internal bool ChangeCustomerPassword(string newPassword,int id)
        {
            try
            {
                using(SqlConnection con =new SqlConnection(cs))
                {
                    con.Open();
                    string query = "update userdetail set Password=@password where Id=@id ";
                    using(SqlCommand cmd=new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@password", newPassword);
                        cmd.Parameters.AddWithValue("@id", id);
                       int rowAffect= cmd.ExecuteNonQuery();

                        if (rowAffect > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return status;
        }

        internal bool UpdateUserDetail(daoUser daouser)
        {
            using(SqlConnection con=new SqlConnection(cs))
            {
                con.Open();
                string query = "update userdetail set Name=@name, PhoneNumber=@phone where Id=@id";
                using(SqlCommand cmd=new SqlCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@name", daouser.name);
                    cmd.Parameters.AddWithValue("@phone", daouser.phone);
                    cmd.Parameters.AddWithValue("@id", daouser.id);
                   int rowAffected =cmd.ExecuteNonQuery();
                    if (rowAffected > 0)
                    {
                        status = true;
                    }
                }
            }
            return status;
        }

        internal DataTable GetCustomerDetail(int id)
        {
            try
            {
                using(SqlConnection con=new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from userdetail where Id=@id";
                    using(SqlCommand cmd=new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return dt;
        }

        internal bool DeleteAddtoCart(int id)
        {
            try
            {
                using(SqlConnection con =new SqlConnection(cs))
                {
                    con.Open();
                    string query = "delete from AddtoCart where id=@id";
                    using(SqlCommand cmd=new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        int rowAffect=cmd.ExecuteNonQuery();
                        if (rowAffect > 0)
                        {
                            status = true;
                        }

                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return status;
        }

        internal DataTable getCartDetail(int customerId)
        {
            try
            {
                using(SqlConnection con=new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from AddtoCart where userId=@id";
                    using (SqlCommand cmd=new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", customerId);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt1);
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }

            return dt1;
        }

        internal DataTable viewAllAdmin()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from [userdetail]";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                        
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }


        internal bool customerSignUp(daoUser daouser)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("customerSignUp",con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@name", daouser.name);
                        cmd.Parameters.AddWithValue("@username", daouser.UserName);
                        cmd.Parameters.AddWithValue("@password", daouser.password);
                        cmd.Parameters.AddWithValue("@gender", daouser.gender);
                        cmd.Parameters.AddWithValue("phonrNumber", daouser.phone);
                        cmd.Parameters.AddWithValue("userGroupId", daouser.adminValue);

                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            status = true;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return status;
        }

        internal DataTable getAdminLoginInfo(daoUser user)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("getAdminLoginInfo", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@username", user.UserName);
                        cmd.Parameters.AddWithValue("@password", user.password);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }

                }
            }
            catch
            {
                throw;
            }
            return dt;
        }

        internal bool checkUserName(string username)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("checkUserName", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@username", username);
                        SqlDataReader da = cmd.ExecuteReader();
                        if (da.HasRows == true)
                        {
                            status = true;
                        }
                    }

                }
            }
            catch
            {
                throw;
            }

            return status;
        }
    }
}