﻿using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace OnlineMobileShopManagementSystem.DAL
{
    public class dalAddPhone
    {
        private static readonly string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        DataTable dt=new DataTable();
        bool st = false;
        int rowAffect;
        internal bool insertPhone(daoPhone phone)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into product(Catagory,Brand,Model,Display,Camera,Ram,InternalStorage,Battery,Price,Image) values(@Catagory,@Brand,@Model,@Display,@Camera,@Ram,@InternalStorage,@Battery,@Price,@Image)";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@Catagory", phone.productCatagory);
                        cmd.Parameters.AddWithValue("@Brand", phone.productBrand);
                        cmd.Parameters.AddWithValue("@Model", phone.productModel);
                        cmd.Parameters.AddWithValue("@Display", phone.display);
                        cmd.Parameters.AddWithValue("@Camera", phone.camera);
                        cmd.Parameters.AddWithValue("@Ram", phone.ram);
                        cmd.Parameters.AddWithValue("@InternalStorage", phone.internelStorage);
                        cmd.Parameters.AddWithValue("@Battery", phone.battery);
                        cmd.Parameters.AddWithValue("@Price", phone.price);
                        cmd.Parameters.AddWithValue("@Image", phone.image);


                        int RowAffected = cmd.ExecuteNonQuery();
                        if (RowAffected > 0)
                        {
                            st = true;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return st;
        }

        internal int InsertIntoCart(daoPhone daoPhone)
        {
            try
            {
                using(SqlConnection con=new SqlConnection(cs))
                {
                    con.Open();
                    string query = "insert into AddtoCart(imageUrl,brand,model,price,numberOfItem,userId) values(@imageUrl,@brand,@model,@price,@numberOfItem,@userId)";
                    using(SqlCommand cmd=new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@imageUrl", daoPhone.image);
                        cmd.Parameters.AddWithValue("@brand", daoPhone.productBrand);
                        cmd.Parameters.AddWithValue("@model", daoPhone.productModel);
                        cmd.Parameters.AddWithValue("@price", daoPhone.price);
                        cmd.Parameters.AddWithValue("@numberOfItem", daoPhone.count);
                        cmd.Parameters.AddWithValue("@userId", daoPhone.id);

                         rowAffect=cmd.ExecuteNonQuery();
                        if (rowAffect > 0)
                        {
                            st = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return rowAffect;
        }

        internal DataTable viewSearch(daoPhone daophone)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from product where Brand=@brand";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@brand", daophone.productBrand);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }

                }
            }
            catch(Exception)
            {
                throw;
            }
            return dt;
        }

        internal DataTable viewSearchDetail(daoPhone daoPhone)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from product where Id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", daoPhone.id);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch(Exception)
            {
                throw;
            }
            return dt;
        }

        internal DataTable recentPhone()
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = "select top 5 * from product order by id desc";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                }
            }
            return dt;
        }

        internal DataTable viewAllPhone()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from product";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return dt;
        }

        internal DataTable updatePhone(daoPhone daophone)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from product where Id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", daophone.id);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch(Exception)
            {
                throw;
            }

            return dt;
        }

        internal bool updatePhoneDetail(daoPhone daophone)
        {
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = "update product set Catagory=@catagory,Brand=@brand,Model=@model,Display=@display,Camera=@camera,Ram=@ram,InternalStorage=@internalStorage,Battery=@battery,Price=@price,Image=@image where Id=@id";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@catagory", daophone.productCatagory);
                    cmd.Parameters.AddWithValue("@brand", daophone.productBrand);
                    cmd.Parameters.AddWithValue("@model", daophone.productModel);
                    cmd.Parameters.AddWithValue("@display", daophone.display);
                    cmd.Parameters.AddWithValue("@camera", daophone.camera);
                    cmd.Parameters.AddWithValue("@ram", daophone.ram);
                    cmd.Parameters.AddWithValue("@internalStorage", daophone.internelStorage);
                    cmd.Parameters.AddWithValue("@battery", daophone.battery);
                    cmd.Parameters.AddWithValue("@price", daophone.price);
                    cmd.Parameters.AddWithValue("@image", daophone.image);
                    cmd.Parameters.AddWithValue("@id", daophone.id);

                    int RowAffected = cmd.ExecuteNonQuery();
                    if (RowAffected>0)
                    {
                        st = true;
                    }
                }
            }
            return st;
        }

        internal DataTable searchPhoneAdmin(string searchText)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "select *from product where Catagory=@catagory or Brand=@brand or Model=@model";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@catagory", searchText);
                        cmd.Parameters.AddWithValue("@brand", searchText);
                        cmd.Parameters.AddWithValue("@model", searchText);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch
            {
                throw;
            }
            return dt;
        }

        internal bool deletePhone(int delete)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    string query = "delete from product where Id=@id";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", delete);
                        int rowAffected = cmd.ExecuteNonQuery();
                        if (rowAffected > 0)
                        {
                            st = true;
                        }
                    }
                }
            }
            catch(Exception)
            {
                throw;
            }
            return st;
        }
    }
}