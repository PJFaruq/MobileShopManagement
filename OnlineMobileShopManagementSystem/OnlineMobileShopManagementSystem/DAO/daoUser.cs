﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMobileShopManagementSystem.DAO
{
    public class daoUser
    {
        public int id { get; set; }
        public string name { get; set; }
        public string UserName { get; set; }
        public string password { get; set; }
        public string gender { get; set; }
        public string phone { get; set; }
        public string adminValue { get; set; }
    }
}