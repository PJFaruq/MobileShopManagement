﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineMobileShopManagementSystem.DAO
{
    public class daoPhone
    {
        public int id { get; set; }
        public string productCatagory { get; set; }
        public string productBrand { get; set; }
        public string productModel { get; set; }
        public string display { get; set; }
        public string camera { get; set; }
        public string ram { get; set; }
        public string internelStorage { get; set; }
        public string battery { get; set; }
        public double price { get; set; }
        public string image { get; set; }
        public int count { get; set; }

    }
}