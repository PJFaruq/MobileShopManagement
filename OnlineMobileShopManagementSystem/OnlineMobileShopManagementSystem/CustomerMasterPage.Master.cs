﻿using OnlineMobileShopManagementSystem.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem
{
    public partial class Customer : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                lblAddtoCart.Text = (string)Session["NumberofCart"];
            }
            
            DataTable dt = new DataTable();
            bllUser user = new bllUser();
            int customerId = Convert.ToInt32(Session["CustomerId"]);
            dt = user.getCartDetail(customerId);
            lblAddtoCart.Text = dt.Rows.Count.ToString();
            


            if (!IsPostBack)
            {
                
                //string check = (string)Session["user"];
                //if (check == null)
                //{
                //    Response.Redirect("~/UI/StartLoginPage.aspx");
                //}
                string customerLogin = (string)Session["CustomerLogin"];
                if (customerLogin == "true")
                {
                    loginPanel.Visible = false;
                    logoutPanel.Visible = true;
                }
                if (customerLogin == null)
                {
                    loginPanel.Visible = true;
                    logoutPanel.Visible = false;
                }
            }

        }

        protected void btnAsus_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Asus");
        }

        protected void btnXiaomi_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Xiaomi");
        }

        protected void btnHTC_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=HTC");
        }

        protected void btnSony_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Sony");
        }

        protected void btnWalton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Walton");
        }

        protected void btnSymphony_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Symphony");
        }

        protected void btnOppo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Oppo");
        }

        protected void btnHuawei_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Huawei");
        }

        protected void btnNokia_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Nokia");
        }

        protected void btnSamsung_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=Samsung");
        }

        protected void btniPhone_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/ViewSearch.aspx?brand=iPhone");
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/CustomerSignUpForm.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/CustomerLogin.aspx");
        }

        protected void btnPhoneSearch_Click(object sender, EventArgs e)
        {
            if (txtSearchPhone.Text == "")
            {
                Response.Redirect("CustomerHome.aspx");
            }
            Session["viewSearchFromHome"] = txtSearchPhone.Text;
            Response.Redirect("HomeSearch.aspx");
            
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session["CustomerLogin"] = null;
            Response.Redirect("~/UI/Customer/CustomerHome.aspx");
        }

        protected void btnAccount_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Customer/CustomerAccountDetail.aspx");
        }

        protected void AddtoCart()
        {
            
        }

        public Label LabelMasterPage
        {
            get{
                return this.lblAddtoCart;
            }
            
        }


    }
}