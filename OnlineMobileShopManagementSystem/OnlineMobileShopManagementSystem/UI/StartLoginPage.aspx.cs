﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem
{
    public partial class StartLoginPage : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //Username Validation
                if (txtUserName.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter Username";
                    return;
                }

                //Password Validation
                if (txtPassword.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter Password";
                    return;
                }


                //Assign Value
                
                bllUser blluser = new bllUser();
                daoUser user = new daoUser();
                user.UserName = txtUserName.Text;
                user.password = txtPassword.Text;
                dt = blluser.getAdminLoginInfo(user);
                
                //Checking Admin or Customer or Super User
                if(dt.Rows.Count>0)
                {
                    if (dt.Rows[0]["Name"].ToString() == "Admin")
                    {
                        Session["user"] = "Admin";
                        Response.Redirect("~/UI/Admin/AdminHome.aspx");
                    }
                    if (dt.Rows[0]["Name"].ToString() == "Customer")
                    {
                        Session["CustomerLogin"] = "true";
                        Session["user"] = "Customer";
                        Response.Redirect("~/UI/Customer/CustomerHome.aspx");
                    }
                    if (dt.Rows[0]["Name"].ToString() == "Super User")
                    {
                        Session["user"] = "Super User";
                        Response.Redirect("~/UI/Admin/AdminHome.aspx");
                    }
                }
                else
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Incorrect Username or Password";
                }

               
          
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}