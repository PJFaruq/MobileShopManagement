﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewSearch.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.ViewSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
            <div class="col-md-12">
                
                <table class="table">
                <tr>
                    <td class="well" >
                        <asp:Label ID="lblErrorMsg" runat="server" Visible="false" Font-Size="20px" ForeColor="red" Font-Italic="true" ></asp:Label>
                        <asp:Repeater ID="rptrViewSearch" runat="server">
                            <ItemTemplate>
                                <ul class="list-inline pull-left" style="margin-left:2px;">
                                     <li style="padding-left:10px;">
                                    <a href="ViewSearchDetail.aspx?id=<%#Eval("Id")%>" class="btn btn-block">
                                        <img src="<%#Eval("Image")%>" height="180" width="180" />
                                    </a>
                                    <br />
                                    <a href="ViewSearchDetail.aspx?id=<%#Eval("Id")%>" class="btn btn-primary btn-block">
                                        <%#Eval("Brand") %> <%#Eval("Model") %><br />
                                        TK <%#Eval("Price") %>
                                    </a>
                                    <br />
                                </li>
                                </ul>
                               
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            </div>
        </div>



        <%--<div class="wrapperfull">
            <div class="wrapper boxShadow">
               <div style="padding:20px;"><asp:Label ID="lblErrorMsg" runat="server" Visible="false" Font-Size="50px" ForeColor="red" Font-Italic="true" ></asp:Label></div> 
                <div style="margin-left:12px;">
                    <asp:Repeater ID="rptrViewSearch" runat="server">
                    <ItemTemplate>
                        <li class="ViewSearchTemplate">

                               <a href="ViewSearchDetail.aspx?id=<%#Eval("Id") %>"><img src="<%#Eval("Image") %>" height="200" width="200" /></a> <br />
                               <a href="ViewSearchDetail.aspx?id=<%#Eval("Id") %>">
                                <%#Eval("Brand") %> <%#Eval("Model") %><br />
                                TK <%#Eval("Price") %><br />    
                                </a> <br />  
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="clr"></div>
                </div>
                
            </div>
        </div>--%>
</asp:Content>
