﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerLogin.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.CustomerLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../StyleSheet.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
       <div class="col-md-6 col-md-offset-3">
           <div class="headCatagory h4 well text-center" style="margin-bottom:0px">
                    Login Here
                </div>
       </div>
   </div>


    <div class="row">
       <div class="col-md-6 col-md-offset-3">
           <div class="well">
               <div class="row">
        <div class="col-md-12">
            <div style="padding-bottom:10px">
                <asp:Label ID="lblErrorMsg" runat="server"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
                <div class="form">

                <div class="form-group form-group-lg">
                     <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" CssClass="form-control"></asp:TextBox>
                </div>
                    <div class="form-group form-group-lg">
                      <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" CssClass="form-control"></asp:TextBox>
                </div>
                    <div class="form-group form-group-lg">
                     <asp:Button ID="btnLogin" runat="server" Text="Log In" OnClick="btnLogin_Click" CssClass="btn btn-success btn-block btn-lg" />
                </div>
                    </div>
                    <div class="form-group">
                     <div class=" h5 text-center">
                         Don't have an account? <a href="CustomerSignUpForm.aspx" class="alert-link"> Register Here</a>
                     </div>
                </div>
               
            </div>
        </div>
    </div>
           </div>
       </div>
</asp:Content>
