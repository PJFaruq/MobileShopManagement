﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerAccountDetail.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.CustomerAccountDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../StyleSheet.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-3">
            <div class="well">
                <ul class="nav nav-stacked nav-pills h4">
                    <li  class="active"><a href="#AccountDetail" data-toggle="tab">Account Detail</a></li>
                    <li><a href="#EditAccount" data-toggle="tab">Edit Account</a></li>
                    <li><a href="#ChangePassword" data-toggle="tab">Change Password</a></li>
                    <li><a href="#OrderHistory" data-toggle="tab">Order History</a></li>
                    <li><a href="#Transactions" data-toggle="tab">Transactions</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="well">
                <div style="padding-bottom:10px">
                <asp:Label ID="lblErrorMsg" runat="server"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
                <div class="tab-content">
                    <div id="AccountDetail" class="tab-pane active">
                        <div class="Panel panel-primary">
                            <div class="panel-body">
                                <asp:Repeater ID="rptrAccountDetail" runat="server">
                                    <HeaderTemplate>
                                        <table class="table table-bordered table-responsive table-stripe">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="h4">Name :
                                            </td>
                                            <td>
                                                <%#Eval("Name")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="h4">User Name :
                                            </td>
                                            <td>
                                                <%#Eval("UserName")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="h4">Gender :
                                            </td>
                                            <td>
                                                <%#Eval("Gender")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="h4">Phone Number :
                                            </td>
                                            <td>
                                                <%#Eval("PhoneNumber")%>
                                            </td>
                                        </tr>
                                        

                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>

                    <div id="EditAccount" class="tab-pane">
                        <div class="Panel panel-primary">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">User Name</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">Gender</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtGender" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label class="control-label">Phone</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-success btn-block btn-lg" OnClick="btnUpdate_Click" />
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ChangePassword" class="tab-pane">
                        <div class="Panel panel-primary">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label class="control-label">Old Password</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtOldPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label class="control-label">New Password</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label class="control-label">Confirm Password</label>
                                        </div>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <asp:Button ID="btnChangePassword" runat="server" Text="Update" CssClass="btn btn-success btn-block btn-lg" OnClick="btnChangePassword_Click"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="OrderHistory" class="tab-pane">
                        <div class="Panel panel-primary">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>

                    <div id="Transactions" class="tab-pane">
                        <div class="Panel panel-primary">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


</asp:Content>
