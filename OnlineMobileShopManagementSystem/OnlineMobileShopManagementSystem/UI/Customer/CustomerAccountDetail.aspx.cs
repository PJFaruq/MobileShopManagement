﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Customer
{
    public partial class CustomerAccountDetail : System.Web.UI.Page
    {
        
        DataTable dt = new DataTable();
        bllUser blluser = new bllUser();
        daoUser daouser = new daoUser();
        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Session["CustomerId"]);
                if (Session["CustomerLogin"] == null)
                {
                    Response.Redirect("CustomerHome.aspx");
                }

                dt = blluser.GetCustomerDetail(id);
                rptrAccountDetail.DataSource = dt;
                rptrAccountDetail.DataBind();

                txtName.Text = dt.Rows[0]["Name"].ToString();
                txtUserName.Text= dt.Rows[0]["UserName"].ToString();
                txtGender.Text= dt.Rows[0]["Gender"].ToString();
                txtPhoneNumber.Text= dt.Rows[0]["PhoneNumber"].ToString();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["CustomerId"]);
            daouser.name = txtName.Text;
            daouser.phone =txtPhoneNumber.Text;
            daouser.id = id;

            bool st = blluser.UpdateUserDetail(daouser);
            if (st == true)
            {
                Response.Redirect("CustomerAccountDetail.aspx");
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Session["CustomerId"]);
            dt = blluser.GetCustomerDetail(id);

            if (txtOldPassword.Text=="" || txtNewPassword.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Fill All The Field";
                return;
            }
            if(txtOldPassword.Text != dt.Rows[0]["Password"].ToString())
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Your Password is Wrong";
                return;
            }

            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Password not Match";
                return;
            }

            

            string newPassword = txtConfirmPassword.Text;

            bool st = blluser.ChangeCustomerPassword(newPassword,id);
        }
    }
}