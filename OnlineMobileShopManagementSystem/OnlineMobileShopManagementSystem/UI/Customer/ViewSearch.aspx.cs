﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Customer
{
    public partial class ViewSearch : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        bllAddPhone bllPhone = new bllAddPhone();
        daoPhone daoPhone = new daoPhone();
                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string viewSearchBrand = Request.QueryString["brand"];
                if (viewSearchBrand != null)
                {
                    viewSearchFromBrand(viewSearchBrand);
                }
                 else
                 {
                     Response.Redirect("CustomerHome.aspx");
                 }

            }
        }

        private void viewSearchFromBrand(string viewSearchBrand)
        {
            daoPhone.productBrand = viewSearchBrand;
            dt = bllPhone.viewSearch(daoPhone);
            if (dt.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "No Phone Available";
            }
            rptrViewSearch.DataSource = dt;
            rptrViewSearch.DataBind();
        }


    }
}