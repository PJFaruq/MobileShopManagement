﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="CustomerSignUpForm.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.CustomerSignUpForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <div class="row">
       <div class="col-md-6 col-md-offset-3">
           <div class="headCatagory h4 well text-center" style="margin-bottom:0px">
                    Register Here
                </div>
       </div>
   </div>
   <div class="row">
       <div class="col-md-6 col-md-offset-3">
           <div class="well">
               <div class="row">
        <div class="col-md-12">
            <div style="padding-bottom:10px">
                <asp:Label ID="lblErrorMsg" runat="server"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
                <div class="form">

                <div class="form-group form-group-lg">
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"  placeholder="Full Name"></asp:TextBox>
                </div>
                <div class="form-group form-group-lg">
                     <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control"  placeholder="User Name"></asp:TextBox>
                </div>

                <div class="form-group form-group-lg">
                     <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"  placeholder="Password" TextMode="Password"></asp:TextBox>
                </div>

                <div class="form-group form-group-lg">
                     <asp:DropDownList ID="ddlGender" CssClass="form-control"  runat="server">
                            <asp:ListItem Value="0" Text="Select One" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                            <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                        </asp:DropDownList>
                </div>

                <div class="form-group form-group-lg">
                     <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control"  placeholder="Phone Number"></asp:TextBox>
                </div>

                <div class="form-group">
                     <asp:Button ID="btnSignUp" runat="server" CssClass="btn btn-success btn-block btn-lg" Text="Sign Up" OnClick="btnSignUp_Click"  />
                </div>

            </div>
        </div>
    </div>
           </div>
       </div>
   </div>
        
    
    

</asp:Content>
