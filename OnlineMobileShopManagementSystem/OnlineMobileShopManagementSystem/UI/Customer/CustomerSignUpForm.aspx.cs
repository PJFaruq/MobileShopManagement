﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Customer
{
    public partial class CustomerSignUpForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            bool st = false;
            daoUser daouser = new daoUser();
            bllUser blluser = new bllUser();

            //Name Validation
            if (txtName.Text == "")
            {

                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter Your Name";
                return;
            }

            //Username Validation
            if (txtUserName.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter The Username";
                return;
            }

            //Password Validation
            if (txtPassword.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Enter the Password";
                return;
            }
            if (txtPassword.Text.Length < 4)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Your Password is too Short";
                return;
            }

            //Gender Validation
            if (ddlGender.SelectedValue == "0")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Please Select Your Gender";
                return;
            }
            
            //Phone Validation
            if (txtPhoneNumber.Text == "")
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text="Please Enter the Phone Number";
                return;
            }
            int checkInt;
            if(!int.TryParse(txtPhoneNumber.Text,out checkInt))
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text="Please a Valid Phone Number";
                return;
            }

            //Checking Username Exist
            string username = txtUserName.Text;
            st = blluser.checkUserName(username);
            if (st == true)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Username is Already Exist";
                return;
            }

            daouser.name = txtName.Text;
            daouser.UserName = txtUserName.Text;
            daouser.password = txtPassword.Text;
            daouser.gender = ddlGender.SelectedValue;
            daouser.adminValue = "2";
            daouser.phone = txtPhoneNumber.Text;

            st = blluser.customerSignUp(daouser);
            if (st == true)
            {
                Response.Redirect("CustomerLogin.aspx");
            }

        }
    }
}