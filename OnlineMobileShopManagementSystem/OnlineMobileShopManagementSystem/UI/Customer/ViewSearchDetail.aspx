﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewSearchDetail.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.ViewSearchDetail" %>
<%@ MasterType VirtualPath="~/CustomerMasterPage.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="well" style="background-color: #ffffff">
        <div style="padding-bottom:10px">
                <asp:Label ID="lblErrorMsg" runat="server"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
            </div>
        <asp:Repeater ID="rptrViewSearchDetail" runat="server">
            <HeaderTemplate>
            </HeaderTemplate>
            <ItemTemplate>
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <img src="<%#Eval("Image")%>" height="350" width="350" />
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:LinkButton ID="btnAddtoCart" runat="server" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnAddtoCart_Click">
                                    <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                                        </asp:LinkButton>
                                    </div>

                                    <div class="col-md-6">
                                        <asp:LinkButton ID="btnBuyNow" runat="server" CssClass="btn btn-primary btn-lg btn-block">
                                    <span class="glyphicon glyphicon-usd"></span> Buy Now
                                        </asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <table class="table table-bordered table-hover" style="height: 415px">
                            <tr>
                                <td colspan="2" style="background-color: #f5f5f5">
                                    <div class="h3 text-center"><%#Eval("Brand") %> <%#Eval("Model") %></div>
                                </td>
                            </tr>

                            <tr>
                                <td>Display</td>
                                <td><%#Eval("Display") %>"</td>
                            </tr>
                            <tr>
                                <td>Camera</td>
                                <td><%#Eval("Camera") %> Megapixels</td>
                            </tr>
                            <tr>
                                <td>Ram</td>
                                <td><%#Eval("Ram") %> GB</td>
                            </tr>
                            <tr>
                                <td>Internal Storage</td>
                                <td><%#Eval("InternalStorage") %> GB</td>
                            </tr>
                            <tr>
                                <td>Battery</td>
                                <td><%#Eval("Battery") %> mAh</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>BDT. <%#Eval("Price") %></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>




</asp:Content>
