﻿using OnlineMobileShopManagementSystem.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI
{
    public partial class CustomerHome : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                recentPhone();
            }
        }

        private void recentPhone()
        {
            bllAddPhone bllPhone=new bllAddPhone();
            dt = bllPhone.recentPhone();
            rptrRecentPhone.DataSource = dt;
            rptrRecentPhone.DataBind();
        }
        
    }
}