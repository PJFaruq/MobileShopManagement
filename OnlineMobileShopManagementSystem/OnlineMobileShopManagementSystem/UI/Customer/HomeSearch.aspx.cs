﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Customer
{
    public partial class HomeSearch : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        bllAddPhone bllPhone = new bllAddPhone();
        daoPhone daoPhone = new daoPhone();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string viewSearchHome = (string)Session["viewSearchFromHome"];
                if (viewSearchHome != null)
                {
                    viewSearchFromeHome(viewSearchHome);
                }

                if (viewSearchHome == null)
                {
                    Response.Redirect("CustomerHome.aspx");
                }
            }
        }

        private void viewSearchFromeHome(string viewSearchHome)
        {
            dt = bllPhone.searchPhoneAdmin(viewSearchHome);
            if (dt.Rows.Count <= 0)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "No Phone Available";
            }
            rptrViewSearch.DataSource = dt;
            rptrViewSearch.DataBind();
        }
    }
}