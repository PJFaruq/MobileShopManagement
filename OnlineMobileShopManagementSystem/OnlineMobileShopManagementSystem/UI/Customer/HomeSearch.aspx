﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="HomeSearch.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.HomeSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <div class="row">
            <div class="col-md-12">
                
                <table class="table">
                <tr>
                    <td class="well" >
                        <asp:Label ID="lblErrorMsg" runat="server" Visible="false" Font-Size="20px" ForeColor="red" Font-Italic="true" ></asp:Label>
                        <asp:Repeater ID="rptrViewSearch" runat="server">
                            <ItemTemplate>
                                <ul class="list-inline pull-left" style="margin-left:2px;">
                                     <li style="padding-left:10px;">
                                    <a href="ViewSearchDetail.aspx?id=<%#Eval("Id")%>" class="btn btn-block">
                                        <img src="<%#Eval("Image")%>" height="180" width="180" />
                                    </a>
                                    <br />
                                    <a href="ViewSearchDetail.aspx?id=<%#Eval("Id")%>" class="btn btn-primary btn-block">
                                        <%#Eval("Brand") %> <%#Eval("Model") %><br />
                                        TK <%#Eval("Price") %>
                                    </a>
                                    <br />
                                </li>
                                </ul>
                               
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            </div>
        </div>             
    

</asp:Content>
