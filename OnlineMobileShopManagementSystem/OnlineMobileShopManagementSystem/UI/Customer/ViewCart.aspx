﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewCart.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Customer.ViewCart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="h3 text-center">Cart List</div>
                </div>

                <div class="panel-body">
                    <div style="padding-bottom: 10px">
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
                    </div>
                    <asp:Repeater ID="rptrViewCart" runat="server">
                        <HeaderTemplate>
                            <table class="table table-hover table-bordered table-responsive">
                                <thead class="h4" style="background-color: #187406; color: white">
                                    <tr>
                                        <td>Image</td>
                                        <td>Brand</td>
                                        <td>Model</td>
                                        <td>Price</td>
                                        <td colspan="2">Action</td>
                                    </tr>
                                </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="<%#Eval("imageUrl")%>" height="50" width="50" alt="phone pic" />
                                    </td>
                                    <td>
                                        <%#Eval("brand")%>
                                    </td>
                                    <td>
                                        <%#Eval("model")%>
                                    </td>
                                    <td>
                                        <%#Eval("price")%>
                                    </td>
                                    <td colspan="2">
                                        <a href="ViewCart.aspx" class="btn btn-primary">
                                            <span class="glyphicon glyphicon-usd"></span>Buy Now
                                        </a>
                                        <a href="ViewCart.aspx?delid=<%#Eval("id") %>" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-remove"></span>Remove
                                        </a>
                                    </td>

                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
