﻿using OnlineMobileShopManagementSystem.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Customer
{
    public partial class ViewCart : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        bllUser user = new bllUser();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["CustomerLogin"] == null)
                {
                    Response.Redirect("CustomerHome.aspx");
                }
                try
                {
                    int id = Convert.ToInt32(Request.QueryString["delid"]);
                    if (id != null)
                    {
                        DeleteAddtoCart(id);
                    }


                    int customerId = Convert.ToInt32(Session["CustomerId"]);
                    dt = user.getCartDetail(customerId);
                    if (dt.Rows.Count <= 0)
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = "Your Cart is Empty";
                    }
                    rptrViewCart.DataSource = dt;
                    rptrViewCart.DataBind();

                    

                    
                }
                catch(Exception ex)
                {
                    throw;
                }
            }
            
        }

        private void DeleteAddtoCart(int id)
        {
            bool st=false;
            st = user.DeleteAddtoCart(id);
        }
    }
}