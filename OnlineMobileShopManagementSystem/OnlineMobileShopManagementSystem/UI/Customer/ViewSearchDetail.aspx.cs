﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Customer
{
    public partial class ViewSearchDetail : System.Web.UI.Page
    {
        int id;
        int rowAffect;
        bllAddPhone bllAddPhone = new bllAddPhone();
        bllUser blluser = new bllUser();
        daoPhone daoPhone = new daoPhone();
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Convert.ToInt32((string)Request.QueryString["id"]);
            daoPhone.id = this.id;
            dt = bllAddPhone.viewSearchDetail(daoPhone);
            rptrViewSearchDetail.DataSource = dt;
            rptrViewSearchDetail.DataBind();
            
        }

        protected void btnAddtoCart_Click(object sender, EventArgs e)
        {
            if(((string)Session["CustomerLogin"] == null)){
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "You have to Login First";
                return;
            }
            

            daoPhone.image = dt.Rows[0]["Image"].ToString();
            daoPhone.productBrand= dt.Rows[0]["Brand"].ToString();
            daoPhone.productModel= dt.Rows[0]["Model"].ToString();
            daoPhone.price=Convert.ToDouble( (string)dt.Rows[0]["Price"]);
            daoPhone.count = 0;
            daoPhone.id = Convert.ToInt32(Session["CustomerId"]);



            rowAffect = bllAddPhone.InsertIntoCart(daoPhone);

            int customerId = Convert.ToInt32(Session["CustomerId"]);
            dt = blluser.getCartDetail(customerId);
            Master.LabelMasterPage.Text = (dt.Rows.Count).ToString();
            

        }
    }
}