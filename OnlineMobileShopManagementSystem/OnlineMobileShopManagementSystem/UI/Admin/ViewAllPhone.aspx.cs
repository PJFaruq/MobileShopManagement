﻿using OnlineMobileShopManagementSystem.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Admin
{
    public partial class ViewAllPhone : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                lblmsg.Visible = false;
                
                
            }
            
            if (!IsPostBack)
            {
                try
                {
                    int delete = Convert.ToInt32((string)Request.QueryString["Delid"]);
                    if (delete != null)
                    {
                        deletePhone(delete);
                    }

                    string msg = Request.QueryString["name"];
                    if (msg == "update")
                    {
                        lblmsg.Visible = true;
                        lblmsg.Text = "Update Successfully.....";
                    }
                    DataTable dt = new DataTable();
                    bllAddPhone bllPhone = new bllAddPhone();
                    dt=bllPhone.viewAllPhone();
                    rptrViewAllPhone.DataSource = dt;
                    rptrViewAllPhone.DataBind();
                    

                }
                catch(Exception)
                {
                    throw;
                }
            }
        }

        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            bllAddPhone bllPhone = new bllAddPhone();
            string searchText = txtSearchBox.Text;
            dt = bllPhone.searchPhoneAdmin(searchText);
            if (dt.Rows.Count <= 0)
            {
                lblmsg.Visible = true;
                lblmsg.Text = "No Phone is Found........";
            }
            rptrViewAllPhone.DataSource = dt;
            rptrViewAllPhone.DataBind();
        }

        protected void deletePhone(int delete)
        {
            bool st = false;
            bllAddPhone bllPhone = new bllAddPhone();
          st=  bllPhone.deletePhone(delete);
          if (st == true)
          {
              lblmsg.Visible = true;
              lblmsg.Text = "Phone is Successfully Deleted........";
          }
        }

    }
}