﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Admin
{
    public partial class UpdatePhone : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        daoPhone daophone = new daoPhone();
        string path;
        int id;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                 id = Convert.ToInt32((string)Request.QueryString["id"]);
                DataTable dt = new DataTable();
                bllAddPhone bllphone = new bllAddPhone();
                daophone.id =id;
                dt = bllphone.updatePhone( daophone);


                ddlProductCatagory.SelectedValue = dt.Rows[0]["Catagory"].ToString();
                ddlProductBrand.SelectedValue = dt.Rows[0]["Brand"].ToString();
                txtModel.Text = dt.Rows[0]["Model"].ToString();
                txtDisplay.Text = dt.Rows[0]["Display"].ToString();
                txtCamera.Text = dt.Rows[0]["Camera"].ToString();
                txtRam.Text = dt.Rows[0]["Ram"].ToString();
                txtInternelStorage.Text = dt.Rows[0]["InternalStorage"].ToString();
                txtBattery.Text = dt.Rows[0]["Battery"].ToString();
                txtPrice.Text = dt.Rows[0]["Price"].ToString();
                Session["image"] = dt.Rows[0]["Image"].ToString();




            }
        }

        protected void btnUpdatePhone_Click(object sender, EventArgs e)
        {
            try
            {
                id = Convert.ToInt32((string)Request.QueryString["id"]);
                if (!fileUpImage.HasFile)
                {
                    path = (string)Session["image"];
                }
                if (fileUpImage.HasFile)
                {
                    string name = fileUpImage.PostedFile.FileName;
                    string extension = System.IO.Path.GetExtension(fileUpImage.FileName);
                    if(extension.ToLower()==".jpg" || extension.ToLower() == ".png")
                    {
                        fileUpImage.PostedFile.SaveAs(Server.MapPath(".") + "//ProductImage//" + name);
                         path = "../Admin/ProductImage/" + name.ToString();
                    }

                    else
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = "Only .jpg and .png file Supported";
                        return;
                    }
                }

                daoPhone phone = new daoPhone();
                bllAddPhone bllPhone = new bllAddPhone();
                phone.productCatagory = ddlProductCatagory.SelectedValue;
                phone.productBrand = ddlProductBrand.SelectedValue;
                phone.productModel = txtModel.Text;
                phone.display = txtDisplay.Text;
                phone.camera = txtCamera.Text;
                phone.ram = txtRam.Text;
                phone.internelStorage = txtInternelStorage.Text;
                phone.battery = txtBattery.Text;
                phone.price = Convert.ToDouble(txtPrice.Text);
                phone.image = path;
                phone.id = id;

                bool st = false;
                st = bllPhone.updatePhoneDetail(phone);
                if (st == true)
                {
                    Response.Redirect("ViewAllPhone.aspx?name=update");
                }
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}