﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateAdminInfo.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Admin.UpdateAdminInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
         .updateAdminTable{
    margin: 0px auto;
    font-size:20px;
    background-color:red;
    padding:10px;
    color:white;
    width:400px;
    
    
    }

        .auto-style1 {
            width: 400px;
        }
        .updateAdminHead{
            font-size:30px;
            background-color:red;
            border:3px white solid;
            width:600px;
            margin:0px auto;
            text-align:center;
            color:white;
            margin-top:5px;
            padding:5px;
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapperfull">
        <div class="wrapper ">
    <div class="updateAdminHead">
                Update Admin Info.
            </div>
            <table class="updateAdminTable">
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="black" Font-Bold="true" Font-Italic="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtFullName" runat="server" placeholder="Full Name" Text="<%#Eval("Name") %>" Height="35px" Width="397px" Font-Size="Large"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" Text="<%#Eval("UserName") %>" Height="35px" Width="397px" Font-Size="Large"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" Text="<%#Eval("Password") %>" Height="35px" Width="397px" Font-Size="Large" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:DropDownList ID="ddlGender" runat="server" Height="35px" Width="400px" Font-Size="Large" >
                            <asp:ListItem Value="0" Text="Select Gender"></asp:ListItem>
                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                            <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                         <asp:TextBox ID="txtPhoneNumber" runat="server" placeholder="Phone Number" Height="35px" Width="397px" Font-Size="Large"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Button ID="btnAddAdmin" runat="server" Text="Update" Height="45px" Width="292px" Style="margin-top: 17px; margin-left: 54px;" Font-Size="Large" BackColor="Green" Font-Bold="true" ForeColor="White"/>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</asp:Content>
