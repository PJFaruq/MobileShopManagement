﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewAdmin.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Admin.ViewAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .viewAdminTable {
            width: 100%;
            border-collapse: collapse;
            margin-top: 2px;
        }

            .viewAdminTable td {
                border: 1px black solid;
                padding: 5px;
            }

        .viewAdminTableColumnName {
            text-align: center;
            font-size: large;
            background-color: #3a6070;
            color: white;
        }

        .viewAdminTableHeaderName {
            text-align: center;
            background-color:#ff0000;
            color:white;
            font-size:30px;
            font-family:'Arial Nova';
            font-weight:700;

        }

        .viewAdminTable tr:nth-child(odd) td {
            background-color: #f3eded;
        }

        .viewAdminTable tr:first-child td {
            background-color: #3a6070;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapperfull">
        <div class="wrapper boxShadow">
            <asp:Repeater ID="rptrViewAdmin" runat="server">
                <HeaderTemplate>
                    <table class="viewAdminTable">
                        <tr class="viewAdminTableHeaderName">
                            <td colspan="12">All Admin Details
                            </td>
                        </tr>
                        <tr class="viewAdminTableColumnName">
                            <td>Name
                            </td>
                            <td>User Name
                            </td>
                            <td>Password
                            </td>
                            <td>Gender
                            </td>
                            <td>Phone Number
                                <td colspan="2">Action 
                                </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#Eval("Name")%> 
                        </td>
                        <td>
                            <%#Eval("UserName")%>  
                        </td>
                        <td>
                            <%#Eval("Password")%>  
                        </td>
                        <td>
                            <%#Eval("Gender")%>
                        </td>
                        <td>
                            <%#Eval("PhoneNumber")%>
                        </td>

                        <td>
                            <a href="?id=<%#Eval("id")%>">Edit</a>
                        </td>
                        <td>
                            <a href="#?id=<%#Eval("id")%>">Delete</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
