﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdatePhone.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Admin.UpdatePhone" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="text-center">Update Phone Details</h4>
                </div>
                <div style="padding: 10px 0px 0px 15px">
                    <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
                </div>

                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Product Catagory</label>
                            </div>

                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlProductCatagory" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Text="Select One" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Android" Text="Android"></asp:ListItem>
                                    <asp:ListItem Value="Apple" Text="Apple"></asp:ListItem>
                                    <asp:ListItem Value="Windows" Text="Windows"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Product Brand</label>
                            </div>

                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlProductBrand" runat="server" CssClass="form-control">
                    <asp:ListItem Value="0" Text="Select One" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="iPhone" Text="iPhone"></asp:ListItem>
                    <asp:ListItem Value="Samsung" Text="Samsung"></asp:ListItem>
                    <asp:ListItem Value="Nokia" Text="Nokia"></asp:ListItem>
                    <asp:ListItem Value="Huawei" Text="Huawei"></asp:ListItem>
                    <asp:ListItem Value="Oppo" Text="Oppo"></asp:ListItem>
                    <asp:ListItem Value="Symphony" Text="Symphony"></asp:ListItem>
                    <asp:ListItem Value="Walton" Text="Walton"></asp:ListItem>
                    <asp:ListItem Value="Sony" Text="Sony"></asp:ListItem>
                    <asp:ListItem Value="HTC" Text="HTC"></asp:ListItem>
                    <asp:ListItem Value="Xiaomi" Text="Xiaomi"></asp:ListItem>
                    <asp:ListItem Value="Asus" Text="Asus"></asp:ListItem>

                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Product Model</label>
                            </div>

                            <div class="col-md-8">
                                 <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Display</label>
                            </div>

                            <div class="col-md-8">
                                <asp:TextBox ID="txtDisplay" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Camera</label>
                            </div>

                            <div class="col-md-8">
                                <asp:TextBox ID="txtCamera" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Ram</label>
                            </div>

                            <div class="col-md-8">
                                <asp:TextBox ID="txtRam" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Internel Storage</label>
                            </div>

                            <div class="col-md-8">
                                <asp:TextBox ID="txtInternelStorage" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Battery</label>
                            </div>

                            <div class="col-md-8">
                                <asp:TextBox ID="txtBattery" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Price</label>
                            </div>

                            <div class="col-md-8">
                                <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Image</label>
                            </div>

                            <div class="col-md-8">
                                <asp:FileUpload ID="fileUpImage" runat="server" CssClass="input-md" />
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-8 col-md-offset-3">
                                 <asp:Button ID="btnUpdatePhone" runat="server" Text="Update" CssClass="btn btn-primary btn-block" OnClick="btnUpdatePhone_Click" />
                            </div>
                        </div>
                        
                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>
