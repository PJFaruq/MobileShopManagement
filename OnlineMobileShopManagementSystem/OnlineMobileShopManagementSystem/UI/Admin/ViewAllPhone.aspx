﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewAllPhone.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Admin.ViewAllPhone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="input-group  col-md-6 col-md-offset-3">
                        <asp:TextBox ID="txtSearchBox" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-btn">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-success" OnClick="btnSearch_Click"  />
                        </span>
                    </div>
                </div>
                <div class="panel-body">
                    <asp:Label ID="lblmsg" CssClass="h3 text-center" runat="server" Visible="false"></asp:Label>
                    <asp:Repeater ID="rptrViewAllPhone" runat="server">
                <HeaderTemplate>
                    <table class="table table-bordered table-responsive table-hover">
                        <tr style="background-color:#194607;color:white; font-size:16px">
                            <td>Category
                            </td>
                            <td>Brand
                            </td>
                            <td>Model
                            </td>
                            <td>Display
                            </td>
                            <td>Camera
                            </td>
                            <td>Ram
                            </td>
                            <td>Intenal Storage
                            </td>
                            <td>Battery
                            </td>
                            <td>Price
                            </td>
                            <td>Image
                            </td>
                            <td colspan="2">Action 
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%#Eval("Catagory")%> 
                        </td>
                        <td>
                            <%#Eval("Brand")%>  
                        </td>
                        <td>
                            <%#Eval("Model")%>  
                        </td>
                        <td>
                            <%#Eval("Display")%>
                        </td>
                        <td>
                            <%#Eval("Camera")%>
                        </td>
                        <td>
                            <%#Eval("Ram")%>
                        </td>
                        <td>
                            <%#Eval("InternalStorage")%>
                        </td>
                        <td>
                            <%#Eval("Battery")%>
                        </td>
                        <td>
                            <%#Eval("Price")%>
                        </td>
                        <td>
                            <img src="<%#Eval("Image")%>" height="50" width="50" />
                        </td>
                        <td>
                            <a href="UpdatePhone.aspx?id=<%#Eval("id")%>">Edit</a>
                        </td>
                        <td>
                            <a href="ViewAllPhone.aspx?Delid=<%#Eval("id")%>">Delete</a>
                        </td>
                    </tr>


                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    
</asp:Content>


