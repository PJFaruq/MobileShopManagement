﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AddAdmin.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Admin.AddUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                     <h4 class="text-center">Add An Admin</h4>
                </div>
                <div style="padding:10px 0px 0px 15px"> 
                    <asp:Label ID="lblErrorMsg" runat="server"  ForeColor="Red" Visible="false" Font-Bold="true" Font-Italic="true"></asp:Label>
                </div>
                
                <div class="panel-body">
                    <div class="form-group">
                        <asp:TextBox ID="txtFullName" runat="server" placeholder="Full Name" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                        <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" CssClass="form-control"></asp:TextBox>
                    </div>
                        <div class="form-group">
                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0" Text="Select Gender" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                            <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="txtPhoneNumber" runat="server" placeholder="Phone Number" CssClass="form-control"></asp:TextBox>
                    </div> 
                    <div class="form-group col-md-6 col-md-offset-3">
                        <asp:Button ID="btnAddAdmin" CssClass="btn btn-success btn-block btn-lg" runat="server" Text="Add" OnClick="btnAddAdmin_Click"/>
                    </div>
                    
                    </div>
            </div>
        </div>
    </div>

</asp:Content>
