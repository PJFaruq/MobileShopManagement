﻿using OnlineMobileShopManagementSystem.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Admin
{
    public partial class ViewAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string userType = (string)Session["user"];
                if (userType != "Super User")
                {
                    Response.Redirect("~/ui/Admin/AdminHome.aspx");
                }

                DataTable dt = new DataTable();
                bllUser blluser = new bllUser();
                dt = blluser.viewAllAdmin();
                rptrViewAdmin.DataSource = dt;
                rptrViewAdmin.DataBind();
            }
            

        }
    }
}