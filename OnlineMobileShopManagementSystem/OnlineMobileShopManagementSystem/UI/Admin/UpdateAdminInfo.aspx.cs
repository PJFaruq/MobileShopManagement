﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Admin
{
    public partial class UpdateAdminInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string userType = (string)Session["user"];
                if (userType != "Super User")
                {
                    Response.Redirect("~/ui/Admin/AdminHome.aspx");
                }
            }
        }
    }
}