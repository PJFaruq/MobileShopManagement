﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMasterPage.Master" AutoEventWireup="true" CodeBehind="AdminHome.aspx.cs" Inherits="OnlineMobileShopManagementSystem.UI.Admin.AdminHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="DesignPage.css" rel="stylesheet" type="text/css" />
    <style>
        .imageSlide img {
            border: 2px red solid;
        }
        .imageSlide{
            margin-top:2px;
        }
        .marqueeText{
            border: 2px red solid;
            font-size:30px;
            margin-top:2px;
            color:white;
            background-color:#0094ff;
            text-align:center;
            padding:3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapperfull">
        <div class="wrapper">
            <div class="marqueeText">             
              <marquee >
                  Welcome to the World's smallest Shop of Smart Phone
              </marquee> 
            </div>
           
        </div>
    </div>
    <div class="wrapperfull">
        <div class="wrapper">
            <div class="imageSlide">
                <marquee direction="right">
                <img src="../../Image/1.jpg" height="400" width="400" />
                    <img src="../../Image/2.jpg" height="400" width="400" />
                    <img src="../../Image/3.jpg" height="400" width="400" />
                    <img src="../../Image/4.jpg" height="400" width="400" />
                    <img src="../../Image/5.jpg" height="400" width="400" />
               </marquee>

            </div>
            <div class="clr">
            </div>

        </div>
    </div>
</asp:Content>
