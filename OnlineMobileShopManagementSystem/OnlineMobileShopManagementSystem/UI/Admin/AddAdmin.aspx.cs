﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI.Admin
{
    public partial class AddUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string userType = (string)Session["user"];
            if (userType != "Super User")
            {
                Response.Redirect("~/ui/Admin/AdminHome.aspx");
            }

        }

        protected void btnAddAdmin_Click(object sender, EventArgs e)
        {

            try
            {
                bool status = false;
                daoUser daoUser = new daoUser();
                bllUser blluser=new bllUser();

                //Name Validation
                if (txtFullName.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Full Name";
                    return;
                }

                //UserName Validation
                if (txtUserName.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter The User Name";
                    return;
                }

                //Password Validation
                if (txtPassword.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Password";
                    return;
                }
                if (txtPassword.Text.Length < 4)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Your Password is too Short";
                    return;
                }

                //Gender Validation
                if (ddlGender.SelectedValue == "0")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Select Your Gender";
                        return;
                }

                //Phone Validation
                if(txtPhoneNumber.Text=="")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text="Please Enter Phone Number";
                    return;
                }
                int checkInt;
                if(!int.TryParse(txtPhoneNumber.Text,out checkInt))
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text="Please Enter Valid Phone Number";
                    return;
                }

                //Checking Username Existing
                string username=txtUserName.Text;
                status=blluser.checkUserName(username);
                if(status==true)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text="Username is Already Exist";
                    return;
                }

                daoUser.name = txtFullName.Text;
                daoUser.UserName = txtUserName.Text;
                daoUser.password = txtPassword.Text;
                daoUser.gender = ddlGender.SelectedValue;
                daoUser.phone =txtPhoneNumber.Text;
                daoUser.adminValue = "1";

                status = blluser.addAdmin(daoUser);
                if (status == true)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Admin is Added Successfully";
                }
            }
            catch (Exception ex)
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text= ex.ToString();
            }
        }
    }
}