﻿using OnlineMobileShopManagementSystem.BLL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem.UI
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string name, path;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblErrorMsg.Visible = false;
            }
        }

        protected void btnSaveAddPhone_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlProductCatagory.SelectedValue == "0")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Select a Catagory";
                    return;
                }
                if (ddlProductBrand.SelectedValue == "0")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Select a Band";
                    return;
                }
                if (txtModel.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Model";
                    return;
                }

                if (txtDisplay.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Display Size";
                    return;
                }

                if (!fileUpImage.HasFile)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Upload an Image";
                    return;
                }
                if (txtCamera.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Camera Resulation";
                    return;
                }

                if (txtRam.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Ram Size";
                    return;
                }
                if (txtInternelStorage.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the Internel Storage";
                    return;
                }

                if (txtBattery.Text == "")
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = "Please Enter the the Capacity of Battery";
                    return;
                }

                if (fileUpImage.HasFile)
                {
                    name = fileUpImage.PostedFile.FileName;
                    string extension = System.IO.Path.GetExtension(fileUpImage.FileName);
                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                    {
                        fileUpImage.PostedFile.SaveAs(Server.MapPath(".") + "//ProductImage//" + name);
                        path = "../Admin/ProductImage/" + name.ToString();
                    }
                    else
                    {
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = "Only .jpg and .png file Supported";
                        return;
                    }

                }

                daoPhone phone = new daoPhone();
                bllAddPhone bllPhone = new bllAddPhone();
                phone.productCatagory = ddlProductCatagory.SelectedValue;
                phone.productBrand = ddlProductBrand.SelectedValue;
                phone.productModel = txtModel.Text;
                phone.display = txtDisplay.Text;
                phone.camera = txtCamera.Text;
                phone.ram = txtRam.Text;
                phone.internelStorage = txtInternelStorage.Text;
                phone.battery = txtBattery.Text;
                phone.price = Convert.ToDouble(txtPrice.Text);
                phone.image = path;

                bool st = bllPhone.insertPhone(phone);

                if (st == true)
                {
                    lblErrorMsg.Visible = true;
                    lblErrorMsg.Text = phone.productBrand + " " + phone.productModel + " " + "is Successfully Added.";
                }

                ddlProductCatagory.SelectedValue = "0";
                ddlProductBrand.SelectedValue = "0";
                txtModel.Text = "";
                txtDisplay.Text = "";
                txtCamera.Text = "";
                txtRam.Text = "";
                txtInternelStorage.Text = "";
                txtPrice.Text = "";
                txtBattery.Text = "";


            }
            catch (Exception ex)
            {
                lblErrorMsg.Text = ex.ToString();
            }
        }


    }
}