﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StartLoginPage.aspx.cs" Inherits="OnlineMobileShopManagementSystem.StartLoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Start Login Page</title>
    <style>
        body{
            background-color:#0094ff;
            font-size:30px;
        }
    .start{
    margin: 0px auto;
    font-size:20px;
    background-color:red;
    padding:10px;
    color:white;
    width:400px;
    border:2px white solid;
    border-top:none;
    
    
    }
    .adminPanelHead{
            font-size:30px;
            background-color:red;
            border:3px white solid;
            width:600px;
            margin:0px auto;
            text-align:center;
            color:white;
            margin-top:150px;
            padding:5px;
            font-weight:bold;
        }

        .auto-style1 {
            width: 400px;
        }
    </style>
</head>
<body >
    <form id="form1" runat="server">
        <div class="adminPanelHead">
                Admin Panel
            </div>
        <table class="start">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblErrorMsg" runat="server"  ForeColor="black" Font-Bold="true" Font-Italic="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name" Height="35px" Width="397px" Font-Size="Large"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" Height="35px" Width="397px" Font-Size="Large" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td class="auto-style1">
                    <asp:Button ID="btnLogin" runat="server" Text="Log In"  Height="45px" Width="292px" OnClick="btnLogin_Click" style="margin-top: 17px; margin-left: 54px;" Font-Size="Large" BackColor="Green" Font-Bold="true" ForeColor="White" />
                </td>
            </tr>
            
        </table>
        
    </form>
</body>
</html>
