﻿using OnlineMobileShopManagementSystem.DAL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace OnlineMobileShopManagementSystem.BLL
{
    public class bllUser
    {
        bool status = false;
        dalUser daluser = new dalUser();
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        internal bool addAdmin(daoUser daoUser)
        {
            status = daluser.addAdmin(daoUser);
            return status;
        }

        internal DataTable viewAllAdmin()
        {
            dt=daluser.viewAllAdmin();
            return dt;
        }

        internal bool customerSignUp(daoUser daouser)
        {
           status= daluser.customerSignUp(daouser);
            return status;
        }

        internal DataTable GetCustomerDetail(int id)
        {
            return dt =daluser.GetCustomerDetail(id);
        }

        internal DataTable getAdminLoginInfo(daoUser user)
        {
            dt = daluser.getAdminLoginInfo(user);
            return dt;
        }

        internal bool checkUserName(string username)
        {
            status=daluser.checkUserName(username);
            return status;
        }

        internal DataTable getCartDetail(int customerId)
        {
            dt1 = daluser.getCartDetail(customerId);
            return dt1;
        }

        internal bool DeleteAddtoCart(int id)
        {
            return status =daluser.DeleteAddtoCart(id);
        }

        internal bool UpdateUserDetail(daoUser daouser)
        {
            return status = daluser.UpdateUserDetail(daouser);
        }

        internal bool ChangeCustomerPassword(string newPassword,int id)
        {
            return status= daluser.ChangeCustomerPassword(newPassword,id);
        }
    }
}