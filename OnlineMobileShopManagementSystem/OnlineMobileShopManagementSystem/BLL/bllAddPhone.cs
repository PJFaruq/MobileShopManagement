﻿using OnlineMobileShopManagementSystem.DAL;
using OnlineMobileShopManagementSystem.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace OnlineMobileShopManagementSystem.BLL
{
    public class bllAddPhone
    {
        dalAddPhone dalPhone = new dalAddPhone();
        int rowAffect;
        bool st = false;
        DataTable dt = new DataTable();
        internal bool insertPhone(daoPhone phone)
        {
           
            st = dalPhone.insertPhone(phone);
            return st;
        }

        internal DataTable viewSearch(daoPhone daophone)
        {
            dt = dalPhone.viewSearch(daophone);
            return dt;
        }

        internal DataTable viewSearchDetail(daoPhone daoPhone)
        {
            dt = dalPhone.viewSearchDetail(daoPhone);
            return dt;
        }

        internal DataTable recentPhone()
        {
            dt = dalPhone.recentPhone();
            return dt;
        }

        internal DataTable viewAllPhone()
        {
            dt=dalPhone.viewAllPhone();
            return dt;
        }

        internal DataTable updatePhone(daoPhone daophone)
        {
            dt = dalPhone.updatePhone(daophone);
            return dt;
        }

        internal bool updatePhoneDetail(daoPhone daophone)
        {
            st = dalPhone.updatePhoneDetail(daophone);
            return st;
        }

        internal DataTable searchPhoneAdmin(string searchText)
        {
            dt = dalPhone.searchPhoneAdmin(searchText);
            return dt;
        }

        internal bool deletePhone(int delete)
        {
            st = dalPhone.deletePhone(delete);
            return st;
        }

        internal int InsertIntoCart(daoPhone daoPhone)
        {
            rowAffect = dalPhone.InsertIntoCart(daoPhone);
            return rowAffect;
        }
    }

   
}