﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OnlineMobileShopManagementSystem
{
    public partial class AdminMasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                string check = (string)Session["user"];
                if (check == null)
                {
                    Response.Redirect("~/UI/Customer/CustomerLogin.aspx");
                }

                if (check == "Admin")
                {
                    superUserPanel.Visible = false;
                }
                if (check == "SuperUser")
                {
                    superUserPanel.Visible = true;
                }
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session["user"] = null;
            Response.Redirect("~/UI/Customer/CustomerHome.aspx");
        }
    }
}