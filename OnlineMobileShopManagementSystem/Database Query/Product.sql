create table product(
Id int primary key identity,
Catagory varchar(50),
Brand varchar(50),
Model varchar(50),
Display varchar(50),
Camera varchar(50),
Ram varchar(50),
InternalStorage varchar(50),
Battery varchar(50),
Price varchar(50),
[Image] varchar(50),
BrandId int foreign key references Brand(Id)
)